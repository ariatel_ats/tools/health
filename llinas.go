// Creado por: Javier Garcia, año 2021
package llinas // https://es.wikipedia.org/wiki/Rodolfo_Llin%C3%A1s

import (
	"encoding/json"
	"time"
	"context"

	"github.com/centrifugal/gocent/v3"
	"gitlab.com/ariatel_ats/tools/logger"
	"gitlab.com/ariatel_ats/tools/rethinkdb_event_handler"
	"golang.org/x/crypto/ssh"
	r "gopkg.in/rethinkdb/rethinkdb-go.v6"
)

var log logger.Logger = logger.Logger{Path: "dispatcher/health.log"}

func init() {
	log.Start(7)
}

type sshInfo struct {
	informationFields  string // Un string de un objeto con los scripts para obtener la info en cada server
	dbSession          *r.Session
	connectionUser     string
	connectionPassword string
	statsTable         string
	ipsTable           string
	credentialsTable   string
}

func ManageServersHealth(dbIp, dbPort, dbName, objData, credentialsTable, statsTable, ipsTable, channel_cent, centrifugoUri string, interval int) {
	session, err := uatu.StartConnection(dbIp, dbPort, dbName)
	if err != nil {
		log.Error.Fatalln("No nos pudimos conectar a la DB")
	}

	connectionSSH := sshInfo{}
	connectionSSH.init(
		credentialsTable, statsTable, ipsTable, objData, session,
	)

	ips := make(map[string]bool)

	ips_iniciales, err := r.Table(connectionSSH.ipsTable).Run(connectionSSH.dbSession)
	var ip_inicial interface{}
	if err != nil {
		session.Close()
		log.Error.Fatalln("Error al traernos las IP's")
	}

	for ips_iniciales.Next(&ip_inicial) {
		ip := ip_inicial.(map[string]interface{})["ip"].(string)
		ip += ":22"
		ips[ip] = true
	}

	ips_iniciales.Close()

	go func() {

		c := gocent.New(gocent.Config{
			Addr: centrifugoUri,
			Key:  "f955849f-c066-42f8-8ec4-37b0c66e8cb1",
		})

		ctx := context.Background()

		for {
			var information []map[string]interface{}
			for ip, _ := range ips {
				data, err := updateIpHealth(connectionSSH, ip)
				if err != nil {
					log.Error.Println(err)
				} else {
					information = append(information, data)
				}
			}

			ch_info, err := json.Marshal(information)
			if err != nil {
				log.Debug.Println(err)
				continue
			}

			_, err = c.Publish(ctx, channel_cent, ch_info)
			if err != nil {
				log.Error.Println("Error calling publish: %v", err)
			}

			time.Sleep(time.Duration(interval) * time.Second)
		}
	}()

	uatu.WatchChanges(
		connectionSSH.ipsTable,
		connectionSSH.dbSession,
		func(new interface{}, old interface{}, session *r.Session) {
			_ = session
			updateIp(connectionSSH, new, old, ips)
		},
		func(new interface{}, session *r.Session) {
			_ = session
			newIp(connectionSSH, new, ips)
		},
		func(old interface{}, session *r.Session) {
			_ = session
			deleteIp(connectionSSH, old, ips)
		},
	)
	session.Close()
}

func (s *sshInfo) init(credentialsTable, statsTable, ipsTable, objData string, session *r.Session) {
	if objData == "" {
		s.informationFields = `{
			"id": "$(hostname -I | awk '{print $1}')",
			"ip": "$(hostname -I | awk '{print $1}')",
			"hdd": "$(df -H /dev/sda1 | tail -1 | awk '{print $2, $3, $4, $5}')",
			"mem": "$(free -h | grep Mem | awk '{print $2, $3, $4}')",
			"load": "$(cat /proc/loadavg | awk '{print $1, $2, $3}')",
			"bw": [$(cat /sys/class/net/ens192/statistics/tx_bytes), $(cat /sys/class/net/ens192/statistics/rx_bytes)],
		}`
	} else {
		s.informationFields = objData
	}
	s.dbSession = session
	s.connectionUser, s.connectionPassword = getCredentials(credentialsTable, session)
	s.statsTable = statsTable
	s.ipsTable = ipsTable
	s.credentialsTable = credentialsTable
}

func getCredentials(tableName string, session *r.Session) (user, pass string) {
	res, err := r.Table(tableName).Run(session)
	if err != nil {
		session.Close()
		log.Error.Fatalln("No podemos acceder a las credenciales")
	}
	defer res.Close()
	var row interface{}
	err = res.One(&row)
	if err == r.ErrEmptyResult {
		session.Close()
		log.Error.Fatalln("No hay un usuario para poder conectarnos")
	}
	if err != nil {
		session.Close()
		log.Error.Fatalln("No podemos acceder a las credenciales")
	}

	user = row.(map[string]interface{})["user"].(string)
	pass = row.(map[string]interface{})["password"].(string)
	return
}

func updateIpHealth(connectionSSH sshInfo, host string) (stats_map map[string]interface{}, err error) {
	client, session, err := connectToHost(connectionSSH, host)
	if err != nil {
		log.Error.Println(err)
		return
	}
	out, err := session.Output(`
		cat <<EOF
			` + connectionSSH.informationFields + `
	`)
	if err != nil {
		client.Close()
		return nil, err
	}

	err = json.Unmarshal(out, &stats_map)
	if err != nil {
		client.Close()
		return nil, err
	}

	_, err = r.Table(connectionSSH.statsTable).Insert(stats_map, r.InsertOpts{
		Conflict: func(id, oldDoc, newDoc r.Term) interface{} { return newDoc },
	}).RunWrite(connectionSSH.dbSession)
	if err != nil {
		client.Close()
		return nil, err
	}

	client.Close()

	return
}

func updateIp(connectionSSH sshInfo, new, old interface{}, ips map[string]bool) {
	deleteIp(connectionSSH, old, ips)
	newIp(connectionSSH, new, ips)
}

func newIp(connectionSSH sshInfo, new interface{}, ips map[string]bool) {
	ip := new.(map[string]interface{})["ip"].(string)
	ip += ":22"
	ips[ip] = true
}

func deleteIp(connectionSSH sshInfo, old interface{}, ips map[string]bool) {
	ip := old.(map[string]interface{})["ip"].(string)
	host := ip + ":22"
	delete(ips, host)
	r.Table(connectionSSH.statsTable).Filter(func(row r.Term) r.Term { return row.Field("ip").Eq(ip) }).Delete().Run(connectionSSH.dbSession)
}

func connectToHost(connectionSSH sshInfo, host string) (*ssh.Client, *ssh.Session, error) {

	sshConfig := &ssh.ClientConfig{
		User: connectionSSH.connectionUser,
		Auth: []ssh.AuthMethod{ssh.Password(connectionSSH.connectionPassword)},
	}
	sshConfig.HostKeyCallback = ssh.InsecureIgnoreHostKey()

	client, err := ssh.Dial("tcp", host, sshConfig)
	if err != nil {
		return nil, nil, err
	}

	session, err := client.NewSession()
	if err != nil {
		client.Close()
		return nil, nil, err
	}

	return client, session, nil
}

func ManageEventsHealth(dbIp, dbPort, dbName, statsTable string, eventHandler func(interface{}, interface{}, *r.Session)) {
	session, err := uatu.StartConnection(dbIp, dbPort, dbName)
	if err != nil {
		log.Error.Fatalln("No nos pudimos conectar a la DB")
	}

	uatu.WatchChangesWithoutActions(
		statsTable,
		session,
		eventHandler,
	)
	session.Close()
}
