module gitlab.com/ariatel_ats/tools/health

go 1.17

require (
	github.com/gorilla/websocket v1.4.2
	gitlab.com/ariatel_ats/tools/logger v0.0.0-20220111152829-247b31e3c6f5
	gitlab.com/ariatel_ats/tools/rethinkdb_event_handler v0.0.0-20211015163915-888f0b58bef0
	golang.org/x/crypto v0.0.0-20210921155107-089bfa567519
	gopkg.in/rethinkdb/rethinkdb-go.v6 v6.2.1
)

require (
	github.com/centrifugal/gocent/v3 v3.1.0 // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/hailocab/go-hostpool v0.0.0-20160125115350-e80d13ce29ed // indirect
	github.com/opentracing/opentracing-go v1.2.0 // indirect
	github.com/sirupsen/logrus v1.8.1 // indirect
	golang.org/x/net v0.0.0-20211014222326-fd004c51d1d6 // indirect
	golang.org/x/sys v0.0.0-20211013075003-97ac67df715c // indirect
	golang.org/x/term v0.0.0-20210927222741-03fcf44c2211 // indirect
	google.golang.org/protobuf v1.27.1 // indirect
	gopkg.in/cenkalti/backoff.v2 v2.2.1 // indirect
)
